package it.dade.mobileapp.activity;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import it.dade.mobileapp.R;
import it.dade.mobileapp.model.ProjectBoard;
import it.dade.mobileapp.model.User;

/**
 * used to show details about the project:
 * title,description,members (here the admin can
 * add members,modify the project etc...)
 */
public class ProjectSettingsActivity extends AppCompatActivity {

    private static final String TAG=ProjectSettingsActivity.class.getCanonicalName();
    ListView listView;
    ProjectBoard projectBoard;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String username = PreferenceManager.getDefaultSharedPreferences(this)
                .getString(getResources().getString(R.string.username_preference), "");
        String email = PreferenceManager.getDefaultSharedPreferences(this)
                .getString(getResources().getString(R.string.email_preference), "");
        String password=PreferenceManager.getDefaultSharedPreferences(this)
                .getString(getResources().getString(R.string.password_preference), "");
        int id=PreferenceManager.getDefaultSharedPreferences(this)
                .getInt(getResources().getString(R.string.id_preference), -1);
        user=new User(id,username,password,email);
        Log.d(TAG,"username:"+user.username);
        //TODO:correctly fetch the ProjectBoard. Maybe fetch from the internet again
        setContentView(R.layout.activity_project_settings);
        /*if(user.isAdmin(projectBoard)) setContentView(R.layout.activity_project_settings_admin);
        else setContentView(R.layout.activity_project_settings);*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        listView=(ListView)findViewById(R.id.project_settings_list_view);
        //getSupportActionBar().setTitle(projectBoard.title);
    }

    private class ProjectSettingsListAdapter extends BaseAdapter {

        private static final int TYPE_PROJECT_HEADER=0;
        private static final int TYPE_MEMBER_LIST=1;
        private static final int TYPE_FOOTER_BUTTON=2;

        @Override
        public int getCount() {
            return 0;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            return null;
        }
        @Override
        public int getItemViewType(int position) {
            if(position==0) return TYPE_PROJECT_HEADER;
            else if(position==1) return TYPE_MEMBER_LIST;
            return TYPE_FOOTER_BUTTON;
        }
        @Override
        public int getViewTypeCount() {
            return 2;
        }
    }


}
