package it.dade.mobileapp.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import it.dade.mobileapp.R;
import it.dade.mobileapp.fragment.AddSectionFragment;
import it.dade.mobileapp.fragment.AddTaskFragment;
import it.dade.mobileapp.fragment.ProgressFragmentDialog;
import it.dade.mobileapp.fragment.ProjectFragment;
import it.dade.mobileapp.fragment.SectionContextMenuFragment;
import it.dade.mobileapp.model.ProjectBoard;
import it.dade.mobileapp.model.Section;
import it.dade.mobileapp.model.Task;
import it.dade.mobileapp.model.User;
import it.dade.mobileapp.rest.DatabaseRestInterface;
import it.dade.mobileapp.utils.RecyclerItemClickListener;

public class ListProjectsActivity extends AppCompatActivity implements
        AddTaskFragment.onSubmitListener,
        AddSectionFragment.onSubmitListener,
        ProjectFragment.OnFragmentInteractionListener,
        SectionContextMenuFragment.onSubmitListener {

    public static final String PROJECTS="projects";
    private static final String INDEX="index";
    public static final String QUERY = "query1";
    public static final String QUERY2 = "query2";
    private static final String TAG = ListProjectsActivity.class.getCanonicalName();
    public static final String USER ="user" ;
    private MenuItem menuItem;
    private DrawerLayout drawerLayout;
    private RecyclerView recyclerView;
    //list of the projects name, to put in the NavigationDrawer
    private String[] projects;
    private View previousClickedView;
    User currentUser;
    int currentViewIndex;
    public ProjectBoard projectBoard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "main thread id:" + android.os.Process.getThreadPriority(android.os.Process.myTid()));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String username = PreferenceManager.getDefaultSharedPreferences(this)
                .getString(getResources().getString(R.string.username_preference), "");
        String email = PreferenceManager.getDefaultSharedPreferences(this)
                .getString(getResources().getString(R.string.email_preference), "");
        String password=PreferenceManager.getDefaultSharedPreferences(this)
                .getString(getResources().getString(R.string.password_preference), "");
        int id=PreferenceManager.getDefaultSharedPreferences(this)
                .getInt(getResources().getString(R.string.id_preference),-1);
        currentUser=new User(id,username,password,email);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close
        );
        drawerLayout.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle.syncState();
        recyclerView = (RecyclerView) findViewById(R.id.RecyclerView);
        // use a linear layout manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);
        previousClickedView = null;
        if(savedInstanceState!=null) {
            currentViewIndex=savedInstanceState.getInt(INDEX);
            projects=savedInstanceState.getStringArray(PROJECTS);
            if(currentViewIndex>=0) {
                Log.d(TAG, "view turned:index=" + currentViewIndex + " current project=" + projects[currentViewIndex]);
                //TODO:fetch the prjectboard from the cache instead of asking again to the server
                FetchProjectAsyncTask fetchProjectAsyncTask = new FetchProjectAsyncTask();
                fetchProjectAsyncTask.execute(projects[currentViewIndex]);
            }
            recyclerView.setAdapter(new ListProjectsAdapter(projects));
        } else {
            currentViewIndex=-1;
            //fetches the current projects
            FetchProjectListAsyncTask sendRequestAsyncTask = new FetchProjectListAsyncTask();
            sendRequestAsyncTask.execute(currentUser);
        }
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Log.d(TAG, "element clicked at position " + position);
                drawerLayout.closeDrawer(GravityCompat.START);
                //if the user selects one of the projects
                if (position <= projects.length && position != 0) {
                    if (previousClickedView != null) {
                        ((ListProjectsAdapter.RecyclerItemViewHolder) recyclerView.getChildViewHolder(previousClickedView))
                                .projectTitleTextView.setTextColor(getResources().getColor(android.R.color.black));
                    }
                    //fetch the selected project
                    FetchProjectAsyncTask fetchProjectAsyncTask = new FetchProjectAsyncTask();
                    fetchProjectAsyncTask.execute(projects[position - 1]);
                    ((ListProjectsAdapter.RecyclerItemViewHolder) recyclerView.getChildViewHolder(view))
                            .projectTitleTextView.setTextColor(getResources().getColor(R.color.colorAccent));
                    previousClickedView = view;
                    currentViewIndex = position - 1;
                    //TODO:find a way to update the title of the ActionBar
                }
            }
        }));
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(projectBoard!=null) {
                    AddSectionFragment addSectionFragment = new AddSectionFragment();
                    addSectionFragment.show(getFragmentManager(), "new_section");
                    drawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    Toast.makeText(getApplicationContext(),getResources()
                            .getString(R.string.no_project_selected_error),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
            drawerLayout.closeDrawer(GravityCompat.START);
        else
            super.onBackPressed();
    }
    /*
    handles the rotation,
    saving the current state
    of the activity
    */
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(INDEX, currentViewIndex);
        outState.putStringArray(PROJECTS, projects);
        super.onSaveInstanceState(outState);
    }
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        menuItem = menu.findItem(R.id.search);
        final SearchView searchView =
                (SearchView) menuItem.getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint(Html.fromHtml("<font color = #ffffff>" +
                getResources().getString(R.string.search_hint) + "</font>"));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                listQuestions(query, query);
                menuItem.collapseActionView();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        } else if(id==R.id.action_new_project) {
            //TODO:code to create a new project
        }else if(id==R.id.action_logout) {
            SharedPreferences sharedPreferences =
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            sharedPreferences.edit()
                    .remove(getResources().getString(R.string.username_preference))
                    .remove(getResources().getString(R.string.email_preference)).apply();
            finish();
        } else if(id==R.id.action_project_settings) {
            Intent intent=new Intent(this,ProjectSettingsActivity.class);
            if(projectBoard!=null) {
                intent.putExtra(PROJECTS, projectBoard);
                //TODO:store the projectBoard in the cache
                startActivity(intent);
            }
            else Toast.makeText(this,getResources()
                    .getString(R.string.no_project_selected_error),Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    private void listQuestions(String query, String query2) {
        Intent intent = new Intent(this, ListQuestionsActivity.class);
        intent.putExtra(QUERY, query);
        intent.putExtra(QUERY2, query2);
        startActivity(intent);
    }

    //TODO:for all these add/remove methods, signal to the adapter the change
    @Override
    public void addTask(Task t,int sectionIndex) {
        Log.d(TAG, projectBoard.toString());
        //request to the database,returning task ID
        //t.id=returned value
        projectBoard.sections.get(sectionIndex).addTask(t);
        getFragmentManager().beginTransaction()
                .replace(R.id.content_frame, ProjectFragment.newInstance(projectBoard, currentUser))
                .commit();
        Log.d(TAG, projectBoard.toString());
    }

    @Override
    public void addSection(Section s) {
        Log.d(TAG, projectBoard.toString());
        //request to DB,returning section ID
        //s.id=returned value
        projectBoard.addSection(s);
        getFragmentManager().beginTransaction()
                .replace(R.id.content_frame, ProjectFragment.newInstance(projectBoard, currentUser))
                .commit();
    }

    @Override
    public void removeSection(int sectionIndex) {
        Log.d(TAG,projectBoard.toString());
        //request to the server
        projectBoard.removeSection(sectionIndex);
        getFragmentManager().beginTransaction()
                .replace(R.id.content_frame, ProjectFragment.newInstance(projectBoard, currentUser))
                .commit();
        Log.d(TAG, projectBoard.toString());
    }

    @Override
    public void removeTask(int sectionIndex, int taskIndex) {
        Log.d(TAG,projectBoard.toString());
        //request to the server
        projectBoard.sections.get(sectionIndex).tasks.remove(taskIndex);
        getFragmentManager().beginTransaction()
                .replace(R.id.content_frame,ProjectFragment.newInstance(projectBoard,currentUser))
                .commit();
        Log.d(TAG, projectBoard.toString());
    }


    /*
    * controller used to populate
    * the RecyclerView inside the
    * NavigationDrawer
    */
    private class ListProjectsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private String[] projects;
        private static final int OTHER_ENTRIES = 0;
        private static final int TYPE_HEADER = 2;
        private static final int TYPE_ITEM = 1;
        private static final int TYPE_FOOTER = 3;

        public ListProjectsAdapter(String[] projects) {
            this.projects = projects;
        }

        public class RecyclerHeaderViewHolder extends RecyclerView.ViewHolder {
            public TextView usernameTextView;
            public TextView emailTextView;
            public de.hdodenhof.circleimageview.CircleImageView imageView;

            public RecyclerHeaderViewHolder(View itemView) {
                super(itemView);
                //sets the textViews with the preferences
                usernameTextView = (TextView) itemView.findViewById(R.id.username_textview);
                emailTextView = (TextView) itemView.findViewById(R.id.email_textview);
                imageView = (de.hdodenhof.circleimageview.CircleImageView) itemView.findViewById(R.id.circleView);
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_timer_auto));
                usernameTextView.setText(currentUser.username);
                emailTextView.setText(currentUser.email);
            }
        }

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public class RecyclerItemViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            public TextView projectTitleTextView;

            public RecyclerItemViewHolder(View v) {
                super(v);
                projectTitleTextView = (TextView) v.findViewById(R.id.project_title);
            }
        }
        public class RecyclerFooterViewHolder extends RecyclerView.ViewHolder {
            public TextView footerTextView;

            public RecyclerFooterViewHolder(View v) {
                super(v);
                footerTextView = (TextView) v.findViewById(R.id.footer_textView);
            }
        }
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == TYPE_ITEM) {
                // create a new view
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.project_custom_list_item, parent, false);
                // set the view's size, margins, paddings and layout parameters
                return new RecyclerItemViewHolder(v);
            } else if (viewType == TYPE_HEADER) {
                final View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.project_header_custom_list_item, parent, false);
                return new RecyclerHeaderViewHolder(view);
            } else if (viewType == TYPE_FOOTER) {
                final View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.project_footer_custom_list_item, parent, false);
                return new RecyclerFooterViewHolder(view);
            }
            throw new RuntimeException("There is no type that matches the type " + viewType + " + make sure your using types    correctly");
        }
        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (!isPositionHeader(position) && !isPositionFooter(position)) {
                RecyclerItemViewHolder holder2 = (RecyclerItemViewHolder) holder;
                holder2.projectTitleTextView.setText(projects[(position - 1)]);
            } else if (position == projects.length + 1) {
                RecyclerFooterViewHolder holder3 = (RecyclerFooterViewHolder) holder;
                holder3.footerTextView.setText(getResources().getString(R.string.add_project));
            } else if (position == projects.length + 2) {
                RecyclerFooterViewHolder holder3 = (RecyclerFooterViewHolder) holder;
                holder3.footerTextView.setText(getResources().getString(R.string.logout));
            }
        }
        @Override
        public int getItemCount() {
            return projects.length + OTHER_ENTRIES + 1;
        }
        //added a method that returns viewType for a given position
        @Override
        public int getItemViewType(int position) {
            if (isPositionHeader(position))
                return TYPE_HEADER;
            else if (isPositionFooter(position))
                return TYPE_FOOTER;
            return TYPE_ITEM;
        }
        //added a method to check if given position is a header
        private boolean isPositionHeader(int position) {
            return position == 0;
        }
        private boolean isPositionFooter(int position) {
            return (position >= projects.length + 1);
        }
    }
    private class FetchProjectListAsyncTask extends AsyncTask<User, Void, String[]> {
        DatabaseRestInterface databaseRestInterface;
        protected void onPreExecute() {
            databaseRestInterface = DatabaseRestInterface.getInstance(getApplicationContext());
        }
        @Override
        protected String[] doInBackground(User... users) {
            Log.d(TAG, "doInBackground thread id:" + android.os.Process.getThreadPriority(android.os.Process.myTid()));
            try {
                return databaseRestInterface.fetchProjectList(users[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(final String[] list) {
            projects = list;
            //simply set the adapter of the RecyclerView to show the projects
            recyclerView.setAdapter(new ListProjectsAdapter(projects));
            /*
            FetchProjectAsyncTask fetchProjectAsyncTask=new FetchProjectAsyncTask();
            fetchProjectAsyncTask.execute(projects[0]);
            Here can be something like:
            to fetch directly the first project or a dummy fragment
            that explains where are the projects and asks to select one
            */
        }
    }
    private class FetchProjectAsyncTask extends AsyncTask<String,Void,ProjectBoard> {
        DatabaseRestInterface databaseRestInterface;
        ProgressFragmentDialog progressFragmentDialog;
        protected void onPreExecute() {
            databaseRestInterface = DatabaseRestInterface.getInstance(getApplicationContext());
        }
        @Override
        protected ProjectBoard doInBackground(String... strings) {
            String title=strings[0];
            progressFragmentDialog=new ProgressFragmentDialog();
            progressFragmentDialog.show(getFragmentManager(),"progress");
            try {
                return databaseRestInterface.fetchProject(currentUser,title);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(ProjectBoard pb) {
            //add the correct fragment, based on the index of currentIndexView
            projectBoard=pb;
            progressFragmentDialog.dismiss();
            //insert the fragment
            getFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, ProjectFragment.newInstance(projectBoard, currentUser))
                    .commit();
        }
    }
}