package it.dade.mobileapp.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.support.v7.widget.SearchView;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.dade.mobileapp.R;
import it.dade.mobileapp.fragment.AnswerContextMenuFragment;
import it.dade.mobileapp.fragment.ErrorFragmentDialog;
import it.dade.mobileapp.fragment.ListAnswersFragment;
import it.dade.mobileapp.fragment.ListQuestionsFragment;
import it.dade.mobileapp.fragment.ProgressFragmentDialog;
import it.dade.mobileapp.model.Answer;
import it.dade.mobileapp.model.Question;
import it.dade.mobileapp.rest.StackExchangeRestInterface;

/**
 * this activity shows the list of
 * questions and answers to a question clicked by the user
 * .Should receive information about the query by an intent
 */
public class ListQuestionsActivity extends AppCompatActivity
        implements ListQuestionsFragment.OnFragmentInteractionListener,
        ErrorFragmentDialog.OnFragmentInteractionListener,
        AnswerContextMenuFragment.onSubmitListener{

    private static final String TAG = ListQuestionsActivity.class.getCanonicalName();
    private static final String WHICH_FRAGMENT = "which_fragment" ;
    private ProgressFragmentDialog progressFragment;
    //TODO:update the name of these variables
    private String query1;
    private String query2;
    private boolean onAnswers;
    private SendQuestionAsyncTask sendQuestionAsyncTask;
    private Toolbar toolbar;
    private MenuItem menuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_questions);
        //if it's the first time I start the activity
        progressFragment = new ProgressFragmentDialog();
        //retrieves the toolbar
        toolbar=(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        //do this stuff only if it's the first time the activity was called
        if(savedInstanceState==null) {
            query1 = getIntent().getStringExtra(ListProjectsActivity.QUERY);
            query2 = getIntent().getStringExtra(ListProjectsActivity.QUERY2);
            Log.d(TAG, "query:" + query1 + " " + query2);
            onAnswers=false;
            //executes the query in a separate thread
            sendQuestionAsyncTask = new SendQuestionAsyncTask();
            sendQuestionAsyncTask.execute(query1, query2);
            setTitle(query1);
            //show a progress fragment
            progressFragment.show(getFragmentManager(), "progress");
        } else {
            onAnswers=savedInstanceState.getBoolean(WHICH_FRAGMENT);
            //if there is the answerFragment, then no toolbar should be displayed
            if(onAnswers) hideToolbar();
        }
    }

    /*
        handles the rotation,
        saving the current state
        of the activity
     */
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(WHICH_FRAGMENT, onAnswers);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list_questions, menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        menuItem=menu.findItem(R.id.search);
        final SearchView searchView =
                (SearchView) menuItem.getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint(Html.fromHtml("<font color = #ffffff>" +
                getResources().getString(R.string.search_hint) + "</font>"));
        return true;
    }

    /**
     * when the activity is called another time,
     * (so when I submit a search in the searchView).
     * this will not create another activity,but
     * calls this method
     * @param intent the intent
     */
    @Override
    protected void onNewIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            menuItem.collapseActionView();
            setTitle(query);
            Log.d(TAG, "search submitted with query:"+query);
            sendQuestionAsyncTask = new SendQuestionAsyncTask();
            sendQuestionAsyncTask.execute(query, query);
            //show a progress fragment
            progressFragment.show(getFragmentManager(), "progress");
        }
    }
    @Override
    public void showAnswers(Question question) {
        //Now that I have the object, I can change th fragment and show the answers
        Log.d(TAG, "Show answers called with title:" + question.title);
        //send the request to another thread and substitute the current fragment
        RetrieveAnswerAsyncTask retrieveAnswerAsyncTask=new RetrieveAnswerAsyncTask();
        retrieveAnswerAsyncTask.execute(question);
    }

    @Override
    public void retryClicked() {
        //try again
        sendQuestionAsyncTask=new SendQuestionAsyncTask();
        sendQuestionAsyncTask.execute(query1, query2);
    }

    @Override
    public void cancelClicked() {
        //simply ends the activity
        finish();
    }
    @Override
    public void onBackPressed() {
        //if there is the answers fragment
        if(onAnswers) {
            getFragmentManager().popBackStack();
            onAnswers=false;
            //shows the toolbar
            if(toolbar!=null) showToolbar();
        }
        else {
            //stop the AsyncTask if running
            if(sendQuestionAsyncTask!=null)
                sendQuestionAsyncTask.cancel(true);
            super.onBackPressed();
        }
    }

    @Override
    public void attachAnswer(int index) {
        Log.d(TAG,"attached answer:"+index);
    }

    /*
    handles the multithreading:
    the request is executed inside another
    thread wrt the main thread.
    */
    private class SendQuestionAsyncTask extends AsyncTask<String, Void, List<Question>> {

        //object that allows us to interact with stackExchange
        private StackExchangeRestInterface stackExchangeRestInterface;

        @Override
        protected void onPreExecute() {
            stackExchangeRestInterface=StackExchangeRestInterface
                    .getInstance(getApplicationContext());
        }

        @Override
        protected List<Question> doInBackground(String... strings) {
            try {
                //return stackExchangeRestInterface.fetchQuestions(strings[0],strings[1]);
                try {
                    return stackExchangeRestInterface.fetchQuestions(strings[0], strings[1]);
                } catch (JSONException e) {
                    //if there's no question, then this catch the exception
                    e.printStackTrace();
                }
            } catch (IOException e) {
                popUpError();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final List<Question> list) {
            String result = new String();
            ListQuestionsFragment listQuestionFragment;
            if (list != null) {
                //init the fragment and place it inside the activity
                listQuestionFragment = ListQuestionsFragment.newInstance((ArrayList<Question>) list);
                getFragmentManager().beginTransaction()
                        .setCustomAnimations(R.animator.slide_y_enter_anim, R.animator.slide_y_exit_anim,
                                R.animator.slide_y_pop_enter_anim, R.animator.slide_y_pop_exit_anim)
                        .addToBackStack("question")
                        .replace(R.id.anchor, listQuestionFragment).commit();
            } else {
                //pass an  ArrayList
                listQuestionFragment = ListQuestionsFragment.newInstance(new ArrayList<Question>());
                getFragmentManager().beginTransaction().replace(R.id.anchor, listQuestionFragment).commit();
            }
            //dismiss the dialog if visible
            if(progressFragment!=null ) progressFragment.dismiss();
        }
    }

    private class RetrieveAnswerAsyncTask extends AsyncTask<Question, Void, List<Answer>> {

        //object that allows us to interact with stackExchange
        private StackExchangeRestInterface stackExchangeRestInterface;
        private Question question;

        protected void onPreExecute() {
            stackExchangeRestInterface=StackExchangeRestInterface
                    .getInstance(getApplicationContext());
        }
        @Override
        protected List<Answer> doInBackground(Question... questions) {
            try {
                //passes the id of the question to the stackExchange interface
                try {
                    question=questions[0];
                    return stackExchangeRestInterface.fetchAnswers(questions[0].id);
                } catch (JSONException e) {
                    //if there's no question, then this catch the exception
                    e.printStackTrace();
                }
            } catch (IOException e) {
                popUpError();
            }
            return null;
        }
        @Override
        protected void onPostExecute(final List<Answer> list) {
            String result = new String();
            if (list != null) {
                //init the fragment and place it inside the activity
                ListAnswersFragment listAnswersFragment = ListAnswersFragment
                        .newInstance((ArrayList<Answer>) list, question);
                //hides the toolbar
                if(toolbar!=null) hideToolbar();
                //substitute the fragment,adding it to the back stack
                getFragmentManager().beginTransaction()
                        .addToBackStack("search")
                        .setCustomAnimations(R.animator.slide_x_enter_anim, R.animator.slide_x_exit_anim,
                                R.animator.slide_x_pop_enter_anim,R.animator.slide_x_pop_exit_anim)
                        .replace(R.id.anchor, listAnswersFragment)
                        .commit();
                onAnswers=true;
                //delete the progress fragment
            } else
                showMessage(getResources().getString(R.string.no_answer_message));
        }
    }
    private void popUpError() {
        ErrorFragmentDialog errorFragmentDialog = ErrorFragmentDialog.newInstance(getResources().getString(R.string.error_fragment_title),
                getResources().getString(R.string.error_fragment_message));
        errorFragmentDialog.show(getFragmentManager(),"error");
    }

    private void showMessage(String message) {
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }

    public void hideToolbar() {
        toolbar.animate().
                translationY(-toolbar.getHeight()).setInterpolator(new AccelerateInterpolator(2));
    }
    public void showToolbar() {
        toolbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
    }
}
