package it.dade.mobileapp.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import it.dade.mobileapp.R;
import it.dade.mobileapp.fragment.SettingsFragment;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(savedInstanceState==null) {
            SettingsFragment settingsFragment=new SettingsFragment();
            //adds the fragment of the settings
            getFragmentManager().beginTransaction()
                    .add(R.id.anchor_settings, settingsFragment).commit();
        }
    }

}
