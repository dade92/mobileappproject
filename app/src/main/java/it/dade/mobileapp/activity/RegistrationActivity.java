package it.dade.mobileapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import it.dade.mobileapp.R;
import it.dade.mobileapp.fragment.LoginFragment;
import it.dade.mobileapp.fragment.RegistrationFormFragment;
import it.dade.mobileapp.fragment.RegistrationFragment;

public class RegistrationActivity extends AppCompatActivity
        implements RegistrationFormFragment.OnFragmentInteractionListener,
        RegistrationFragment.OnFragmentInteractionListener,
        LoginFragment.OnFragmentInteractionListener {

    private boolean otherFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //important:sets the default preferences of the app the very first time it's called
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(this);
        //if the user is not registered yet (in the phone,not in our server)
        if(sharedPreferences.getString(getResources().getString(R.string.username_preference),"").equals("")) {
            setContentView(R.layout.activity_registration);
                getFragmentManager().beginTransaction()
                        .replace(R.id.registration_fragment_anchor, new RegistrationFragment()).commit();
                otherFragment = false;

        } else {
            //if the user is already registered
            startTeamFlow();
        }
    }

    @Override
    public void onBackPressed() {
        if(otherFragment) {
            getFragmentManager().popBackStack();
            otherFragment=false;
        }
        else finish();
    }

    @Override
    public void buttonPressed() {
        otherFragment=true;
    }

    /*
    * Callback method
    */
    @Override
    public void startTeamFlow() {
        Intent intent= new Intent(this,ListProjectsActivity.class);
        startActivity(intent);
        finish();
    }
}
