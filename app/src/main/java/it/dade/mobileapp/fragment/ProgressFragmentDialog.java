package it.dade.mobileapp.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.os.Bundle;

import it.dade.mobileapp.R;


/**
 * fragment used to display something
 * when connecting to internet
 */
public class ProgressFragmentDialog extends DialogFragment {

    public ProgressFragmentDialog() {
        // Required empty public constructor
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //create and return a progressDialog
        ProgressDialog progressDialog=new ProgressDialog(getActivity());
        //sets the default message (something like "wait...")
        progressDialog.setMessage(getResources().getString(R.string.progress_fragment_message));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        return progressDialog;
    }
/*
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

}
