package it.dade.mobileapp.fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import it.dade.mobileapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrationFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private static final String TAG=RegistrationFragment.class.getCanonicalName();

    public RegistrationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_registration, container, false);
        //listeners for the buttons
        ((Button)v.findViewById(R.id.registration_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.buttonPressed();
                getActivity().getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.registration_fragment_anchor, new RegistrationFormFragment())
                        .setCustomAnimations(R.animator.fade_enter_anim, R.animator.fade_exit_anim,
                                R.animator.slide_y_pop_enter_anim, R.animator.slide_y_pop_exit_anim)
                        .addToBackStack("registration")
                        .commit();
            }
        });
        ((Button)v.findViewById(R.id.login_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.buttonPressed();
                getActivity().getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.registration_fragment_anchor, new LoginFragment())
                        .setCustomAnimations(R.animator.fade_enter_anim, R.animator.fade_exit_anim,
                                R.animator.slide_y_pop_enter_anim, R.animator.slide_y_pop_exit_anim)
                        .addToBackStack("login")
                        .commit();
            }
        });
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(TAG,"fragment attached");
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void buttonPressed();
    }

}
