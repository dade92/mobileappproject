package it.dade.mobileapp.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import it.dade.mobileapp.R;
import it.dade.mobileapp.model.ProjectBoard;
import it.dade.mobileapp.model.Task;
import it.dade.mobileapp.model.User;
import it.dade.mobileapp.utils.PositionBundle;

/**
 * This fragment contains the project
 * information
 */
public class ProjectFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String PROJECT_BOARD="project_board";
    private static final String USER="user";
    private static final String TAG=ProjectFragment.class.getCanonicalName();

    //declared public,maybe the activity has to change something of the projectBoard,such as the tasks
    public ProjectBoard projectBoard;
    public User currentUser;
    RecyclerView projectRecyclerView;
    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @param projectBoard project
     * @return A new instance of fragment ProjectFragment.
     */
    public static ProjectFragment newInstance(ProjectBoard projectBoard,User user) {
        ProjectFragment fragment = new ProjectFragment();
        Bundle args = new Bundle();
        args.putParcelable(PROJECT_BOARD,projectBoard);
        args.putParcelable(USER,user);
        fragment.setArguments(args);
        return fragment;
    }

    public ProjectFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null){
            projectBoard=getArguments().getParcelable(PROJECT_BOARD);
            currentUser=getArguments().getParcelable(USER);
            Log.d(TAG,"project user 0:"+projectBoard.members.get(0));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_project, container, false);
        projectRecyclerView=(RecyclerView)v.findViewById(R.id.project_recycler_view);
        if(projectBoard.sections!=null) projectRecyclerView.setAdapter(new ListProjectAdapter());
        // use a linear layout manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        projectRecyclerView.setLayoutManager(mLayoutManager);
        return v;
    }

    public class ListProjectAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        /* Provide a reference to the views for each data item
        Complex data items may need more than one view per item, and
        you provide access to all the views for a data item in a view holder*/
        public class RecyclerItemViewHolder extends RecyclerView.ViewHolder
                implements View.OnCreateContextMenuListener {
            TextView sectionTitleTextView;
            TextView sectionDescriptionTextView;
            ImageButton addTaskImageButton;
            LinearLayout taskListGroup;
            public RecyclerItemViewHolder(View itemView) {
                super(itemView);
                itemView.setMinimumWidth(getScreenResolution(getActivity()));
                taskListGroup=(LinearLayout)itemView.findViewById(R.id.task_list_group);
                sectionTitleTextView=(TextView)itemView.findViewById(R.id.section_title);
                sectionDescriptionTextView=(TextView)itemView.findViewById(R.id.section_description);
                addTaskImageButton=((ImageButton)itemView.findViewById(R.id.add_task_button));
                addTaskImageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AddTaskFragment addTaskFragment = AddTaskFragment.newInstance((int) view.getTag());
                        addTaskFragment.show(getFragmentManager(), "new_task");
                    }
                });
                itemView.setOnCreateContextMenuListener(this);
            }

            @Override
            public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
                int index=projectRecyclerView.getChildPosition(view);
                String title=projectBoard.sections.get(index).title;
                //custom context menu
                SectionContextMenuFragment sectionContextMenuFragment= SectionContextMenuFragment
                        .newInstance(title, index);
                sectionContextMenuFragment.show(getFragmentManager(),"context_menu");
            }
        }
        // Create new views (invoked by the layout manager)
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.section_custom_list_item, parent, false);
            return new RecyclerItemViewHolder(v);
        }
        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            RecyclerItemViewHolder holder2 = (RecyclerItemViewHolder) holder;
            LayoutInflater inflater=getActivity().getLayoutInflater();
            //retrieve the tasks of the section
            holder2.sectionTitleTextView.setText(projectBoard.sections.get(position).title);
            holder2.sectionDescriptionTextView.setText(projectBoard.sections.get(position).description);
            holder2.addTaskImageButton.setTag(position);
            ImageButton doneButton;
            int i=0;
            //TODO:use a ListView
            //for each task in the section of the project, add a View
            for(Task t:projectBoard.sections.get(position).tasks) {
                Log.d(TAG,"position:"+position);
                View v=inflater.inflate(R.layout.layout_task_detail,null);
                doneButton=((ImageButton)v.findViewById(R.id.task_done_button));
                PositionBundle positionBundle=new PositionBundle(position,i);
                doneButton.setTag(positionBundle);
                i++;
                doneButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PositionBundle positionBundle=(PositionBundle)view.getTag();
                        mListener.removeTask(positionBundle.sectionIndex,positionBundle.taskIndex);
                        //TODO:refresh the view
                    }
                });
                ((TextView)v.findViewById(R.id.task_title)).setText(t.title);
                ((TextView)v.findViewById(R.id.task_object)).setText(t.object);
                //((TextView)v.findViewById(R.id.task_deadline)).setText(t.deadline.toString());
                holder2.taskListGroup.addView(v);
            }
        }

        @Override
        public int getItemCount() {
            return projectBoard.sections.size();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    private static int getScreenResolution(Context context)
    {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        return width;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void removeTask(int sectionIndex,int taskIndex);
    }

}
