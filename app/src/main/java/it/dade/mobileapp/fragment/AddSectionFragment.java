package it.dade.mobileapp.fragment;


import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import it.dade.mobileapp.R;
import it.dade.mobileapp.model.Section;
import it.dade.mobileapp.model.Task;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddSectionFragment extends DialogFragment {


    Button mButton;
    private static final String TAG=AddTaskFragment.class.getCanonicalName();

    onSubmitListener mListener;

    public interface onSubmitListener {
        void addSection(Section s);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (onSubmitListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.setContentView(R.layout.fragment_add_section);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        mButton = (Button) dialog.findViewById(R.id.new_section_submit_button);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title, description;
                title = ((EditText) dialog.findViewById(R.id.task_title)).getEditableText().toString();
                description = ((EditText) dialog.findViewById(R.id.section_description)).getEditableText().toString();
                mListener.addSection(new Section(title,description));
                dismiss();
            }
        });
        return dialog;
    }

}
