package it.dade.mobileapp.fragment;


import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import it.dade.mobileapp.R;
import it.dade.mobileapp.model.Task;
import it.dade.mobileapp.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddTaskFragment extends DialogFragment {
    Button mButton;
    private static final String TAG=AddTaskFragment.class.getCanonicalName();
    private static final String SECTION_INDEX="section_index";
    private static final String VIEW_GROUP_ID="view_group";
    private int sectionIndex;
    private int viewGroupId;
    onSubmitListener mListener;

    public static AddTaskFragment newInstance(int sectionIndex) {
        AddTaskFragment fragment = new AddTaskFragment();
        Bundle args = new Bundle();
        args.putInt(SECTION_INDEX, sectionIndex);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            sectionIndex=getArguments().getInt(SECTION_INDEX);
        }
    }


    public interface onSubmitListener {
        void addTask(Task t,int sectionIndex);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (onSubmitListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.setContentView(R.layout.fragment_add_task);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        mButton = (Button) dialog.findViewById(R.id.new_task_submit_button);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title,object;
                int priority;
                String deadline=((EditText)dialog.findViewById(R.id.task_date)).getText().toString();
                title=((EditText)dialog.findViewById(R.id.task_title)).getEditableText().toString();
                object=((EditText)dialog.findViewById(R.id.task_object)).getEditableText().toString();
                priority=((SeekBar)dialog.findViewById(R.id.seekBar1)).getProgress();
                Log.d(TAG,"value of the seekbar:"+priority);
                try {
                    //TODO:refresh the view
                    mListener.addTask(new Task(title,object,priority, Utils.parseDateString(deadline)),sectionIndex);
                } catch (ParseException e) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.format_date_error), Toast.LENGTH_SHORT).show();
                }
                dismiss();
            }
        });
        return dialog;
    }

}
