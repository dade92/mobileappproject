package it.dade.mobileapp.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import it.dade.mobileapp.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SectionContextMenuFragment.onSubmitListener} interface
 * to handle interaction events.
 * Use the {@link SectionContextMenuFragment#} factory method to
 * create an instance of this fragment.
 */
public class SectionContextMenuFragment extends DialogFragment {
    onSubmitListener mListener;
    private static final String TITLE="title";
    private static final String INDEX="index";
    private static final String TAG=SectionContextMenuFragment.class.getCanonicalName();
    private String title;
    private int sectionIndex;
    public interface onSubmitListener {
        void removeSection(int sectionIndex);
    }

    public SectionContextMenuFragment() {

    }

    public static SectionContextMenuFragment newInstance(String title,int index) {
        SectionContextMenuFragment contextMenuFragment=new SectionContextMenuFragment();
        Bundle args = new Bundle();
        args.putString(TITLE, title);
        args.putInt(INDEX,index);
        contextMenuFragment.setArguments(args);
        return contextMenuFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null) {
            title=getArguments().getString(TITLE);
            sectionIndex=getArguments().getInt(INDEX);
        }
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (onSubmitListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.setContentView(R.layout.fragment_context_menu);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        ListView listView= (ListView) dialog.findViewById(R.id.section_list_context_menu);
        ((TextView)dialog.findViewById(R.id.context_menu_title)).setText(title);
        final String values[]=getResources().getStringArray(R.array.section_context_menu);
        listView.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return values.length;
            }

            @Override
            public Object getItem(int i) {
                return values[i];
            }

            @Override
            public long getItemId(int i) {
                return 0;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                view=getActivity().getLayoutInflater().inflate(R.layout.context_menu_list_item,null);
                ((TextView)view.findViewById(R.id.simple_textview)).setText(values[i]);
                return view;
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    mListener.removeSection(sectionIndex);
                    Log.d(TAG, "removing section:" + title);
                }
                dismiss();
            }
        });
        return dialog;
    }

}
