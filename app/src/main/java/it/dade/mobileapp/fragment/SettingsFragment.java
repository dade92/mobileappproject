package it.dade.mobileapp.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import it.dade.mobileapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends PreferenceFragment {

    public SettingsFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(getActivity());
        (findPreference(getResources().getString(R.string.username_preference)))
                .setSummary(sharedPreferences.getString(getResources().getString(R.string.username_preference),""));
        (findPreference(getResources().getString(R.string.email_preference)))
                .setSummary(sharedPreferences.getString(getResources().getString(R.string.email_preference),""));
    }
}