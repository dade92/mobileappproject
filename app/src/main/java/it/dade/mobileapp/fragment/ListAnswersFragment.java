package it.dade.mobileapp.fragment;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import it.dade.mobileapp.R;
import it.dade.mobileapp.model.Answer;
import it.dade.mobileapp.model.Question;
import it.dade.mobileapp.rest.StackExchangeRestInterface;
import it.dade.mobileapp.utils.Utils;

//TODO:think a clever way to show the answers in this fragment
/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ListAnswersFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListAnswersFragment extends Fragment {

    private static final String ANSWER= "answers";
    private static final String TAG=ListAnswersFragment.class.getCanonicalName();
    private static final String QUESTION = "question";
    private ArrayList<Answer> answers;
    private Question question;
    private ScrollView bodyScrollView;
    private RecyclerView mRecyclerView;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment ListAnswersFragment.
     */
    public static ListAnswersFragment newInstance(ArrayList<Answer> answers,Question question) {
        ListAnswersFragment fragment = new ListAnswersFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ANSWER,answers);
        args.putParcelable(QUESTION,question);
        fragment.setArguments(args);
        return fragment;
    }

    public ListAnswersFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //gets the answers to be showed
        if (getArguments() != null) {
            answers = getArguments().getParcelableArrayList(ANSWER);
            question=getArguments().getParcelable(QUESTION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view= inflater.inflate(R.layout.fragment_list_answers, container, false);
        //sets the body of the question as a subview
        ((TextView)view.findViewById(R.id.question_body)).setText(question.body);
        //final View dividerView=view.findViewById(R.id.divider);
        bodyScrollView=(ScrollView) view.findViewById(R.id.scroll_view_answer_body);
        bodyScrollView.getLayoutParams().height=question.body.length()>getResources().getDimension(R.dimen.answer_body_height)
                ? (int)getResources().getDimension(R.dimen.answer_body_height) :question.body.length();
        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        Log.d(TAG, "found view");
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        // specify an adapter (see also next example)
        RecyclerView.Adapter mAdapter = new ListAnswerAdapter(answers);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                hideViews();
            }

            @Override
            public void onShow() {
                showViews();
            }
        });
        return view;
    }

    private void hideViews() {
        bodyScrollView.animate().translationY(-bodyScrollView.getHeight()).setInterpolator(new AccelerateInterpolator(1));
    }
    private void showViews() {
        bodyScrollView.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
    }
    /*
    * controller for the RecyclerView
    */
    private class ListAnswerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private ArrayList<Answer> answers;
        //added view types
        private static final int TYPE_HEADER = 2;
        private static final int TYPE_ITEM = 1;

        /*Provide a reference to the views for each data item
            Complex data items may need more than one view per item, and
            you provide access to all the views for a data item in a view holder
        */
        public class RecyclerItemViewHolder extends RecyclerView.ViewHolder
                implements View.OnCreateContextMenuListener {
            // each data item is just a string in this case
            public TextView answerBodyTextView;
            public TextView scoreTextView;
            public TextView dateTextView;
            public RecyclerItemViewHolder(View v) {
                super(v);
                //gets the widgets of the view
                answerBodyTextView = (TextView)v.findViewById(R.id.answer_body);
                scoreTextView=(TextView)v.findViewById(R.id.answer_score);
                dateTextView=(TextView)v.findViewById(R.id.answer_date);
            }

            @Override
            public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
                Log.d(TAG,"creating context menu..");
                AnswerContextMenuFragment answerContextMenuFragment=AnswerContextMenuFragment
                        .newInstance(getResources().getString(R.string.answer),mRecyclerView.getChildPosition(view));
                answerContextMenuFragment.show(getFragmentManager(),"context_menu");
            }
        }
        public class RecyclerHeaderViewHolder extends RecyclerView.ViewHolder {
            public RecyclerHeaderViewHolder(View itemView) {
                super(itemView);
            }
        }

        // Provide a suitable constructor (depends on the kind of dataset)
        public ListAnswerAdapter(ArrayList<Answer> answers) {
            this.answers = answers;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
            if(viewType==TYPE_ITEM) {
                // create a new view
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.answer_custom_list_item, parent, false);
                // set the view's size, margins, paddings and layout parameters

                return new RecyclerItemViewHolder(v);
            } else if(viewType==TYPE_HEADER) {
                final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_header, parent, false);
                return new RecyclerHeaderViewHolder(view);
            }
            throw new RuntimeException("There is no type that matches the type " + viewType + " + make sure your using types correctly");
        }

        //added a method that returns viewType for a given position
        @Override
        public int getItemViewType(int position) {
            if (isPositionHeader(position)) {
                return TYPE_HEADER;
            }
            return TYPE_ITEM;
        }
        //added a method to check if given position is a header
        private boolean isPositionHeader(int position) {
            return position == 0;
        }
        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            if(!isPositionHeader(position)) {
                RecyclerItemViewHolder holder2 = (RecyclerItemViewHolder) holder;
                holder2.answerBodyTextView.setText(answers.get(position - 1).body);
                holder2.scoreTextView.setText(String.valueOf(answers.get(position - 1).score));
                holder2.dateTextView.setText(Utils.parseDate(answers.get(position - 1).creationDate));
            }
        }
        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return answers.size();
        }
    }

    public abstract class HidingScrollListener extends RecyclerView.OnScrollListener {
        private static final int HIDE_THRESHOLD = 20;
        private int scrolledDistance = 0;
        private boolean controlsVisible = true;

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            if (scrolledDistance > HIDE_THRESHOLD && controlsVisible) {
                onHide();
                controlsVisible = false;
                scrolledDistance = 0;
            } else if (scrolledDistance < -HIDE_THRESHOLD && !controlsVisible) {
                onShow();
                controlsVisible = true;
                scrolledDistance = 0;
            }

            if((controlsVisible && dy>0) || (!controlsVisible && dy<0)) {
                scrolledDistance += dy;
            }
        }

        public abstract void onHide();
        public abstract void onShow();

    }

}
