package it.dade.mobileapp.fragment;

import android.app.Activity;
import android.app.ListFragment;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import it.dade.mobileapp.R;
import it.dade.mobileapp.model.Question;
import it.dade.mobileapp.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ListQuestionsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ListQuestionsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListQuestionsFragment extends ListFragment {

    private static final String QUESTIONS = "param1";
    private static final String TAG = ListQuestionsFragment.class.getCanonicalName();

    private ArrayList<Question> questions;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param questions Parameter 1.
     * @return A new instance of fragment ListQuestionsFragment.
     */
    public static ListQuestionsFragment newInstance(ArrayList<Question> questions) {
        ListQuestionsFragment fragment = new ListQuestionsFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(QUESTIONS,questions);
        fragment.setArguments(args);
        return fragment;
    }

    public ListQuestionsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //gets the arraylist of questions
        if (getArguments() != null) questions = getArguments().getParcelableArrayList(QUESTIONS);
        BaseAdapter mBaseAdapter=new BaseAdapter() {
            private static final int HEADER_TYPE=0;
            private static final int ITEM_TYPE=1;

            @Override
            public int getCount() {
                return questions.size();
            }

            @Override
            public Object getItem(int i) {
                return questions.get(i - 1);
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public int getItemViewType(int position) {
                if(position==0) {
                    return HEADER_TYPE;
                }
                return ITEM_TYPE;
            }
            @Override
            public int getViewTypeCount() {
                return 2;
            }
            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                if(view==null) {
                    if(getItemViewType(i)==HEADER_TYPE) {
                        view = getActivity().getLayoutInflater().inflate(R.layout.listview_header, viewGroup, false);
                    }
                    else {
                        view = getActivity().getLayoutInflater().inflate(R.layout.question_custom_list_item, viewGroup, false);
                    }
                }
                if(getItemViewType(i)!=HEADER_TYPE) {
                    //sets the view
                    //sets a tag corresponding to the id of the Question,so I can retrieve later
                    view.setTag(questions.get(i).id);
                    ((TextView) view.findViewById(R.id.question_item_title))
                            .setText(questions.get(i-1).title);
                    //((TextView) view.findViewById(R.id.question_item_score)).setText(String.valueOf(questions.get(i-1).score));
                    ((TextView) view.findViewById(R.id.question_item_date)).setText(Utils.parseDate(questions.get(i - 1).creationDate));
                }
                return view;
            }
        };
        setListAdapter(mBaseAdapter);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mListener.showAnswers(questions.get(i-1));
            }
        });
        getListView().setOnScrollListener(new AbsListView.OnScrollListener() {
            int mLastFirstVisibleItem = 0;
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (view.getId() == getListView().getId()) {
                    final int currentFirstVisibleItem = getListView().getFirstVisiblePosition();
                    if (currentFirstVisibleItem > mLastFirstVisibleItem)
                        mListener.hideToolbar();
                    else if (currentFirstVisibleItem < mLastFirstVisibleItem)
                        mListener.showToolbar();
                    mLastFirstVisibleItem = currentFirstVisibleItem;
                }
            }
        });
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void showAnswers(Question question);
        void showToolbar();
        void hideToolbar();
    }

}
