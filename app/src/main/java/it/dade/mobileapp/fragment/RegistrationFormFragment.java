package it.dade.mobileapp.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import it.dade.mobileapp.R;
import it.dade.mobileapp.rest.DatabaseRestInterface;
import it.dade.mobileapp.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RegistrationFormFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class RegistrationFormFragment extends Fragment {

    private static final String TAG =RegistrationFormFragment.class.getCanonicalName();
    private OnFragmentInteractionListener mListener;

    public RegistrationFormFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v= inflater.inflate(R.layout.fragment_registration_form, container, false);
        //set the listener for the button
        ((Button)v.findViewById(R.id.registration_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username=((TextView)v.findViewById(R.id.username_editText)).getEditableText().toString();
                String password=((TextView)v.findViewById(R.id.password_editText)).getEditableText().toString();
                String email=((TextView)v.findViewById(R.id.email_editText)).getEditableText().toString();
                if(Utils.validateEmail(email)) {
                    if(Utils.validatePassword(password)) {
                        //after this method the password is encrypted
                        password = Utils.sha256Encryption(password);
                        AddUserAsyncTask addUserAsyncTask=new AddUserAsyncTask();
                        addUserAsyncTask.execute(username, password, email);
                    } else Toast.makeText(getActivity(),getResources().getString(R.string.invalid_password_format),Toast.LENGTH_SHORT).show();
                } else Toast.makeText(getActivity(),getResources().getString(R.string.invalid_email_format),Toast.LENGTH_SHORT).show();
            }
        });
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void startTeamFlow();
    }
    private class AddUserAsyncTask extends AsyncTask<String,Void,Boolean> {
        private String username;
        private String password;
        private String email;
        private DatabaseRestInterface databaseRestInterface;
        @Override
        protected void onPreExecute () {
            databaseRestInterface=DatabaseRestInterface.getInstance(getActivity());
        }
        @Override
        protected Boolean doInBackground(String... strings) {
            username=strings[0];
            password=strings[1];
            email=strings[2];
            try {
                Log.d(TAG,"username:"+username);
                Log.d(TAG,"password:"+password);
                Log.d(TAG,"email:"+email);
                return databaseRestInterface.addUser(username,password,email);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }
        @Override
        protected void onPostExecute(Boolean result) {
            if(result) {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                //saves the credential of the user for further accesses
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(getResources().getString(R.string.username_preference), username);
                editor.putString(getResources().getString(R.string.password_preference),password);
                editor.putString(getResources().getString(R.string.email_preference), email);
                editor.apply();
                //pop the activity,returning to the main activity
                mListener.startTeamFlow();
            } else Toast.makeText(getActivity(),
                    getResources().getString(R.string.error_fragment_message),Toast.LENGTH_SHORT).show();
        }
    }

}
