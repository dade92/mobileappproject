package it.dade.mobileapp.utils;

import java.util.Comparator;

import it.dade.mobileapp.model.Answer;

/**
 * Created by dade on 02/01/2016.
 * Comparator for answers. the order is based
 * on the scores of the answers
 */
public class AnswerComparator implements Comparator<Answer> {
    @Override
    public int compare(Answer answer1, Answer answer2) {
        return answer2.score-answer1.score;
    }
}
