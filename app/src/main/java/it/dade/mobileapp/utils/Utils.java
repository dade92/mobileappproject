package it.dade.mobileapp.utils;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.dade.mobileapp.model.Answer;
import it.dade.mobileapp.model.Question;

/**
 * Created by dade on 04/01/2016.
 * Class that contains useful methods
 * to parse strings, or other stuff.
 */
public class Utils {

    private static final String TAG=Utils.class.getCanonicalName();

    /**
     * reads the content of the inputstream and convert it
     * to a string
     * @param stream inputstream
     * @return String representation of the inputStream
     * @throws IOException if the reader cannot convert the inputStream
     */
    public static String readIt(InputStream stream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "ISO-8859-1"), 8);
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = reader.readLine()) != null) {
            sb.append(line + "\n");
        }
        stream.close();

        return sb.toString();
    }
    //parse the response (a string) and returns a json object
    public static List<Question> parseQuestionSearchResponse(String response) throws JSONException {

        List<Question> list = new ArrayList<Question>();
        JSONObject jo = new JSONObject(response);
        JSONArray ja = jo.getJSONArray("items");
        Log.d(TAG, "json array:" + ja);
        //for each question, add it to the list of question objects
        for (int i = 0; i < ja.length(); i++) {
            //this retrieves the single question,finally
            JSONObject j = ja.getJSONObject(i);
            //Log.d(TAG, "Tags:"+j.get("tags"));
            //add the question to the list of questions
            list.add(i,
                    new Question(j.getInt("question_id"), j.getString("title"), j.getString("link"), j.getString("body"), j.getLong("creation_date") * 1000, j.getInt("score")));
        }
        //returns a list sorted by score of the question
        Collections.sort(list, new QuestionComparator());
        return list;
    }

    /*
        parse the answer as string and return
        a list of objects
     */
    public static List<Answer> parseAnswerSearchResponse(String response) throws JSONException {
        List<Answer> list = new ArrayList<Answer>();
        JSONObject jo = new JSONObject(response);
        JSONArray ja = jo.getJSONArray("items");
        JSONObject jo2 = ja.getJSONObject(0);
        JSONArray answers = jo2.getJSONArray("answers");
        //for each answer,add it to the list of answer objects
        for (int i = 0; i < answers.length(); i++) {
            //retrieve the answer
            JSONObject answer = answers.getJSONObject(i);
            //creates an answer object
            list.add(i,
                    new Answer(answer.getInt("answer_id"), answer.getString("body"), answer.getLong("creation_date")*1000, answer.getInt("score")));
        }
        //returns a list sorted by score of answer
        Collections.sort(list, new AnswerComparator());
        return list;
    }

    /**
     * given a url, sends the request to the server
     * and returns a String of the response
     * @param myUrl url
     * @return String representation of the response
     * @throws IOException if connection fails
     */
    public static String returnResponseAsString(String myUrl) throws IOException {
        URL url = new URL(myUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(10000 /* milliseconds */);
        conn.setConnectTimeout(15000 /* milliseconds */);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        // Starts the query
        conn.connect();
        int response = conn.getResponseCode();
        Log.d(TAG, "The response is: " + response);
        InputStream is = conn.getInputStream();
        Log.d(TAG, "input stream:" + is);
        // Convert the InputStream into a string
        return readIt(is);
    }

    public static String sha256Encryption(String text) {
        MessageDigest digest = null;
        //password encryption
        try {
            digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(text.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        }catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String parseDate(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy");
        return format.format(date);
    }
    public static Date parseDateString(String date) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy");
        return format.parse(date);
    }
    public static String parseDateForDB(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yy-MM-dd");
        return format.format(date);
    }
    public static Date parseDateForDB(String date) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yy-MM-dd");
        return format.parse(date);
    }
    public static boolean validateEmail(String email) {
        String emailPattern =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern=Pattern.compile(emailPattern);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    public static boolean validatePassword(String passowrd) {
        String passwordPattern="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,}$";
        Pattern pattern=Pattern.compile(passwordPattern);
        Matcher matcher = pattern.matcher(passowrd);
        return matcher.matches();
    }
}
