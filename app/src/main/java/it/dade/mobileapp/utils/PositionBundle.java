package it.dade.mobileapp.utils;

/**
 * Created by dade on 13/02/2016.
 */
public class PositionBundle {
    public int sectionIndex;
    public int taskIndex;

    public PositionBundle(int sectionIndex,int taskIndex) {
        this.sectionIndex=sectionIndex;
        this.taskIndex=taskIndex;
    }
}
