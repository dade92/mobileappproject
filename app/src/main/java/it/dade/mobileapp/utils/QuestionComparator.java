package it.dade.mobileapp.utils;

import java.util.Comparator;

import it.dade.mobileapp.model.Question;

/**
 * Created by dade on 02/01/2016. This is a
 * comparator for the Question objects
 */
public class QuestionComparator implements Comparator<Question> {

    @Override
    public int compare(Question question1, Question question2) {
        return question2.score-question1.score;
    }
}
