package it.dade.mobileapp.utils;

import java.util.Comparator;

import it.dade.mobileapp.model.Task;

/**
 * Created by dade on 10/02/2016.
 * Comparator for the tasks.
 */
public class TaskComparator implements Comparator<Task> {
    @Override
    public int compare(Task task, Task t1) {
        //date is more important than priority
        if(task.deadline.getTime()>t1.deadline.getTime()) return 1;
        else if(task.deadline.getTime()<t1.deadline.getTime()) return -1;
        return task.priority-t1.priority;
    }
}
