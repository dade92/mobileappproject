package it.dade.mobileapp.rest;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import it.dade.mobileapp.R;
import it.dade.mobileapp.model.Answer;
import it.dade.mobileapp.model.Question;
import it.dade.mobileapp.utils.AnswerComparator;
import it.dade.mobileapp.utils.QuestionComparator;
import it.dade.mobileapp.utils.Utils;

/**
 * Created by dade on 06/12/2015.
 * Class that interfaces with stackExchange
 * services. NOT thread safe, these methods
 * should be called from another thread.
 * It's a singleton
 */
public class StackExchangeRestInterface {

    private static final String TAG = StackExchangeRestInterface.class.getCanonicalName();
    private Context context;
    private static StackExchangeRestInterface stackExchangeRestInterface;

    //private constructor
    private StackExchangeRestInterface(Context context) {
        this.context = context;
    }

    /**
     * factory method
     *
     * @param context context
     * @return the static (unique) instance
     */
    public static StackExchangeRestInterface getInstance(Context context) {
        if (stackExchangeRestInterface == null)
            stackExchangeRestInterface = new StackExchangeRestInterface(context);
        return stackExchangeRestInterface;
    }

    /**
     * submit a query given a title and a question
     * of the user
     *
     * @param title title to search
     * @param body  body to search
     * @return a list of questions
     * @throws IOException
     */
    public List<Question> fetchQuestions(String title, String body) throws IOException, JSONException {
        List<Question> questions=new ArrayList<>();
        //TODO::handle more sites by a for loop
        String myurl = String.format(context.getResources().getString(R.string.questions_search_url), title, body,"unix");
        Log.d(TAG, "URL:" + myurl);
        String contentAsString = Utils.returnResponseAsString(myurl);
        Log.d(TAG, "The content is: " + contentAsString);
        //parse the string and returns a list of questions
        questions.addAll(Utils.parseQuestionSearchResponse(contentAsString));

        return questions;
    }

    /**
     * submit a request for the answer to a specific question
     *
     * @param questionId id of the question
     * @return a list of answers
     * @throws IOException if it cannot parse the file
     */
    public List<Answer> fetchAnswers(int questionId) throws IOException, JSONException {
        List<Answer> answers=new ArrayList<>();
        //TODO:handle here the case of more sites: for cycle by which we fetch the responses
        String myurl = String.format(context.getResources().getString(R.string.answer_search_url), questionId,"unix");
        String contentAsString = Utils.returnResponseAsString(myurl);
        Log.d(TAG, "The content is: " + contentAsString);
        answers.addAll(Utils.parseAnswerSearchResponse(contentAsString));

        return answers;
    }


    public List<String> listSites() {
        return null;
    }
}
