package it.dade.mobileapp.rest;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;

import it.dade.mobileapp.R;
import it.dade.mobileapp.model.ProjectBoard;
import it.dade.mobileapp.model.Section;
import it.dade.mobileapp.model.Site;
import it.dade.mobileapp.model.Task;
import it.dade.mobileapp.model.User;
import it.dade.mobileapp.utils.Utils;

/**
 * Created by dade on 06/01/2016.
 * Class used to interface with the
 * database that contains the list
 * of projects of the user and other
 * information. Singleton
 */
public class DatabaseRestInterface {

    private static final String TAG=DatabaseRestInterface.class.getCanonicalName();
    private static DatabaseRestInterface databaseRestInterface;
    private Context context;
    private HttpURLConnection httpURLConnection;

    private DatabaseRestInterface(Context context) {
        this.context=context;
    }
    public static DatabaseRestInterface getInstance(Context context) {
        if(databaseRestInterface==null)
            databaseRestInterface=new DatabaseRestInterface(context);
        return databaseRestInterface;
    }
    /*TODO:implement methods*/

    /**
     * fetch the list of the projects
     * @param user current user
     * @return string array with the titles of the projects of the user
     * @throws IOException if connection error
     */
    public String[] fetchProjectList(User user) throws IOException {
        URL url=new URL(context.getResources().getString(R.string.fetch_project_list_url));
        String urlParameters  = "username="+user.username+"&password="+user.password;
        String response=this.sendRequest(urlParameters,url);
        String projects[]=null;
        try {
            JSONArray jsonArray=new JSONArray(response);
            projects=new String[jsonArray.length()-1];
            for(int i=1;i<jsonArray.length();i++) {
                JSONObject jsonObject= (JSONObject) jsonArray.get(i);
                projects[i-1]=jsonObject.getString("title");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return projects;
    }
    public ProjectBoard fetchProject(User user,String title) throws IOException {
        URL url=new URL(context.getResources().getString(R.string.fetch_project_url));
        String urlParameters  = "username="+user.username+"&password="+user.password+"&title="+title;
        String response=this.sendRequest(urlParameters,url);
        try {
            JSONArray jsonArray=new JSONArray(response);
            JSONObject projectInfo= (JSONObject) jsonArray.get(0);
            JSONArray sections=jsonArray.getJSONArray(1);
            JSONArray members=jsonArray.getJSONArray(2);
            JSONArray sites=jsonArray.getJSONArray(3);
            ArrayList<User> memberList=new ArrayList<>();
            ArrayList<Site> siteList=new ArrayList<>();
            ArrayList<Section>sectionList=new ArrayList<>();
            for(int i=0;i<sections.length();i++) {
                JSONArray section= (JSONArray) sections.get(i);
                JSONObject sectionInfo= (JSONObject) section.get(0);
                try {
                    sectionList.add(new Section(sectionInfo.getInt("ID"),
                            sectionInfo.getString("title"), sectionInfo.getString("description")));
                    JSONArray sectionTasks = (JSONArray) section.get(1);
                    sectionList.get(i).setTask(sectionTasks);
                }catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (ClassCastException e) {
                    e.printStackTrace();
                }
            }
            for(int i=0;i<members.length();i++) {
                JSONObject j= (JSONObject) members.get(i);
                memberList.add(new User(j));
            }
            for(int i=0;i<sites.length();i++) {
                JSONObject j= (JSONObject) sites.get(i);
                siteList.add(new Site(j));
            }
            ProjectBoard projectBoard=new ProjectBoard(projectInfo.getInt("ID"),projectInfo.getString("title"),
                    projectInfo.getString("description"),projectInfo.getInt("admin_ID"),sectionList,memberList,siteList);
            Log.d(TAG,projectBoard.toString());
            return projectBoard;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * check if the credentials are correct
     * @param username username of the user
     * @param sha256Password sha password of the user
     * @return true if login was successful, false otherwise
     * @throws IOException if connection problems arised
     */
    public User checkUserCredentials(String username,String sha256Password) throws IOException {
        URL url=new URL(context.getResources().getString(R.string.check_user_credentials_url));
        String urlParameters  = "username="+username+"&password="+sha256Password;
        String response=this.sendRequest(urlParameters, url);
        try {
            JSONObject jsonObject=new JSONObject(response);
            return new User(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * adds a new user to the database
     * @param username username
     * @param sha256Password encrypted password
     * @param email email
     * @return true if the user was added successfully,false otherwise
     * @throws JSONException if the json can't be cast correctly
     * @throws IOException if the connection can't be opened
     */
    public boolean addUser(String username,String sha256Password,String email) throws JSONException, IOException {
        URL url=new URL(context.getResources().getString(R.string.add_user_url));
        String urlParameters  = "username="+username+"&password="+sha256Password+"&email="+email;
        String response=sendRequest(urlParameters,url);
        return response.compareTo("1")==1;
    }

    /**
     * adds a new task to the specified project
     * @param user current user
     * @param projectID id of the project
     * @param sectionID section id of the project
     * @param t Task to be added
     * @return true if the Task was added,false otherwise
     * @throws IOException if connection error
     */
    public int addTask(User user,int projectID,int sectionID,Task t) throws IOException {
        URL url=new URL(context.getResources().getString(R.string.add_task_url));
        String urlParameters  = "username="+user.username+"&password="+user.password+
                "&project_id="+projectID+"&title="+t.title+
                "&description="+t.object+"&priority="+t.priority+"&deadline="+Utils.parseDateForDB(t.deadline)+"&section_id="+sectionID;
        String response=sendRequest(urlParameters,url);
        try {
            return Integer.valueOf((new JSONObject(response)).getInt("ID"));
        } catch(NumberFormatException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * remove the specified task
     * @param user current user
     * @param projectID id of the project
     * @param taskID id of the task
     * @return true if the Task was added,false otherwise
     * @throws IOException if connection error
     */
    public boolean removeTask(User user,int projectID,int taskID) throws IOException {
        URL url=new URL(context.getResources().getString(R.string.remove_task_url));
        String urlParameters  = "username="+user.username+"&password="
                +user.password+"&project_id="+projectID+"&task_id="+taskID;
        String response=sendRequest(urlParameters,url);
        return response.compareTo("1")==1;
    }

    /**
     * add a section to the specified project
     * @param user current user
     * @param section section to be added
     * @param projectID id of the project
     * @return true if the Task was added,false otherwise
     * @throws IOException if connection error
     */
    public int addSection(User user,Section section,int projectID) throws IOException {
        //TODO:check this url
        URL url=new URL(context.getResources().getString(R.string.add_section_url));
        String urlParameters  = "username="+user.username+"&password="
                +user.password+"&project_id="+projectID
                +"&title="+section.title+"&description="+section.description+"&project_id="+projectID;
        String response=sendRequest(urlParameters,url);
        try {
            return Integer.valueOf((new JSONObject(response)).getInt("ID"));
        } catch(NumberFormatException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     *
     * @param user
     * @param projectID
     * @param sectionID
     * @return
     * @throws IOException
     */
    public boolean removeSection(User user,int projectID,int sectionID) throws IOException {
        URL url=new URL(context.getResources().getString(R.string.remove_section_url));
        String urlParameters  = "username="+user.username+"&password="
                +user.password+"&project_id="+projectID
                +"&project_id="+projectID+"&section_id="+sectionID;
        String response=sendRequest(urlParameters, url);
        return response.compareTo("1")==1;
    }
    public boolean addMember(ProjectBoard projectBoard,User u) {
        return true;
    }
    public boolean removeMember(ProjectBoard projectBoard,User u) {
        return true;
    }
    public boolean addProject(ProjectBoard projectBoard) {
        return true;
    }
    public boolean removeProject(ProjectBoard projectBoard) {
        return true;
    }
    /**
     * helper method used to avoid code replication
     * @param urlParameters parameters specific of the request
     * @param url url of the script
     * @return the response
     * @throws IOException if connection problems arised
     */
    private String sendRequest(String urlParameters,URL url) throws IOException {
        final byte[] postData       = urlParameters.getBytes();
        int    postDataLength = postData.length;
        //sets initial parameters of HttpUrlConnection
        httpURLConnection= (HttpURLConnection) url.openConnection();
        httpURLConnection.setReadTimeout(10000 /* milliseconds */);
        httpURLConnection.setConnectTimeout(15000 /* milliseconds */);
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        httpURLConnection.setRequestProperty("charset", "utf-8");
        httpURLConnection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);
        (new DataOutputStream( httpURLConnection.getOutputStream())).write(postData);
        int responseCode = httpURLConnection.getResponseCode();
        if(responseCode==200){
            String response=Utils.readIt(httpURLConnection.getInputStream());
            Log.d(TAG,"response:"+response);
            return response;
        }
        return "Connection Error";
    }
}

