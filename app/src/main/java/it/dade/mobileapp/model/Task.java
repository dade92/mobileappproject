package it.dade.mobileapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import it.dade.mobileapp.utils.Utils;

/**
 * Created by dade on 09/02/2016.
 * Class used to represent the tasks
 * of our project.
 */
public class Task implements Parcelable,Serializable {

    int id;
    public String title;
    public String object;
    public int priority;
    public Date deadline;

    public Task(int id,String title,String object) {
        this.id=id;
        this.title=title;
        this.object=object;
    }
    public Task(int id,String title,String object,int priority) {
        this(id,title,object);
        this.priority=priority;
    }
    public Task(int id,String title,String object,int priority,Date deadline) {
        this(id,title,object,priority);
        this.deadline=deadline;
    }
    public Task(String title,String object,int priority,Date deadline) {
        this.title=title;
        this.object=object;
        this.deadline=deadline;
        this.priority=priority;
    }
    public Task(JSONObject task) throws JSONException, ParseException {
        this.id=task.getInt("ID");
        this.title=task.getString("title");
        this.object=task.getString("description");
        this.deadline= Utils.parseDateForDB(task.getString("deadline"));
        this.priority=task.getInt("priority");
    }

    protected Task(Parcel in) {
        title = in.readString();
        object = in.readString();
        priority = in.readInt();
    }

    public static final Creator<Task> CREATOR = new Creator<Task>() {
        @Override
        public Task createFromParcel(Parcel in) {
            return new Task(in);
        }

        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(object);
        parcel.writeInt(priority);
        parcel.writeSerializable(deadline);
    }
    public String toString() {
        String string="TASK "+title+"\n";
        return string;
    }
}
