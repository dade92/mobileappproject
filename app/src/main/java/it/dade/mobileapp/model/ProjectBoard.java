package it.dade.mobileapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dade on 06/01/2016.
 * Class that represents a project.
 */
public class ProjectBoard implements Parcelable {

    public int id;
    public String title;
    public String description;
    public ArrayList<Section> sections;
    public ArrayList<User> members;
    public ArrayList<Site> sites;
    public User admin;
    public int adminID;

    public ProjectBoard(int id,String title,String description) {
        this.id=id;
        this.title=title;
        this.description=description;
        sections=new ArrayList<>();
        members=new ArrayList<>();
        sites=new ArrayList<>();
    }
    public ProjectBoard(int id,String title,String description,int adminID
            ,ArrayList<Section> sections,ArrayList<User> users,ArrayList<Site> sites) {
        this.id=id;
        this.title=title;
        this.description=description;
        this.sections=sections;
        this.members=users;
        this.sites=sites;
        this.adminID=adminID;
    }
    //TODO:implement a useful constructor
    public ProjectBoard() {

    }

    protected ProjectBoard(Parcel in) {
        title = in.readString();
        description = in.readString();
        sections = in.createTypedArrayList(Section.CREATOR);
        members = in.createTypedArrayList(User.CREATOR);
        sites = in.createTypedArrayList(Site.CREATOR);
        admin = in.readParcelable(User.class.getClassLoader());
    }

    public static final Creator<ProjectBoard> CREATOR = new Creator<ProjectBoard>() {
        @Override
        public ProjectBoard createFromParcel(Parcel in) {
            return new ProjectBoard(in);
        }

        @Override
        public ProjectBoard[] newArray(int size) {
            return new ProjectBoard[size];
        }
    };

    public void addSection(Section s) {
        sections.add(s);
    }
    public void removeSection(String title) {
        int i=0;
        for(Section s:sections) {
            if(title.equals(s.title)) {sections.remove(i);break;}
            i++;
        }
    }
    public void removeSection(int sectionIndex) {
        sections.remove(sectionIndex);
    }
    public void addMember(User u) {
        members.add(u);
    }
    public void removeMember(String username) {
        int i=0;
        for(User u:members) {
            if(username.equals(u.username)) {members.remove(i);break;}
            i++;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeSerializable(members);
        parcel.writeSerializable(sections);
        parcel.writeParcelable(admin,PARCELABLE_WRITE_RETURN_VALUE);
    }
    @Override
    public String toString() {
        String string="PROJECT "+title+"\n";
        for(Section s:sections) {
            string+=s;
        }
        for(User u:members) {
            string+=u;
        }
        for(Site s:sites) {
            string+=s;
        }
        return string;
    }
}
