package it.dade.mobileapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by dade on 06/01/2016.
 * Class that represents a user of the
 * system.
 */
public class User implements Parcelable,Serializable{
    public int id;
    public String username;
    public String password;
    public String email;

    public User(int id,String username,String password,String email) {
        this.id=id;
        this.username=username;
        this.password=password;
        this.email=email;
    }
    public User(String username,String email) {
        this.username=username;
        this.email=email;
    }
    public User(JSONObject user) throws JSONException {
        this.id=user.getInt("ID");
        this.username=user.getString("username");
        this.password=user.getString("password");
        this.email=user.getString("email");
    }
    protected User(Parcel in) {
        username = in.readString();
        email = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(username);
        parcel.writeString(email);
        parcel.writeInt(id);
        parcel.writeString(password);
    }
    public boolean isAdmin(ProjectBoard projectBoard) {
        return username.equals(projectBoard.admin.username);
    }
    public String toString() {
        return String.format("username:%s,password:%s,email:%s\n",username,password,email);
    }
}
