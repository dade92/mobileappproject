package it.dade.mobileapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.net.URL;
import java.util.Date;
import java.util.List;

/**
 * Created by dade on 06/12/2015.
 * Represent a question on the
 * StackExchange site
 */
public class Question implements Parcelable {

    //note that public attributes are suggested in android programming
    public int id;
    public String title;
    public String link;
    public String body;
    public Date creationDate;
    public int score;
    public List<String> tags;

    public Question(int id,String title) {
        this.id=id;
        this.title=title;
    }
    public Question(int id,String title,String link) {
        this(id,title);
        this.link=link;
    }
    public Question(int id,String title,String link,String body,long timestamp,int score) {
        this(id,title,link);
        this.body=android.text.Html.fromHtml(body).toString();
        this.creationDate=new Date(timestamp);
        this.score=score;
    }
    protected Question(Parcel in) {
        id = in.readInt();
        title = in.readString();
        link = in.readString();
        body = in.readString();
        score = in.readInt();
        creationDate=new Date(in.readLong());
        tags = in.createStringArrayList();
    }

    public static final Creator<Question> CREATOR = new Creator<Question>() {
        @Override
        public Question createFromParcel(Parcel in) {
            return new Question(in);
        }

        @Override
        public Question[] newArray(int size) {
            return new Question[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        //Parcelable interface requires this stuff
        parcel.writeInt(id);
        parcel.writeString(title);
        parcel.writeString(body);
        parcel.writeInt(score);
        parcel.writeLong(creationDate.getTime());
    }
    public String toString() {
        return String.format("title:%s\nbody:%s\ncreation date:%s\ncreation timestamp:%d\n",title,body,creationDate.toString(),creationDate.getTime());
    }
}
