package it.dade.mobileapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dade on 09/02/2016.
 * This class represent a Section
 * of the project. A section is
 * simply a set of tasks.
 */
public class Section implements Parcelable,Serializable {

    public int id;
    public ArrayList<Task> tasks;
    public String title;
    public String description;

    public Section(int id,String title,String description) {
        this.id=id;
        this.title=title;
        this.description=description;
        tasks=new ArrayList<>();
    }
    public Section(int id,String title,String description,ArrayList<Task> tasks) {
        this.id=id;
        this.title=title;
        this.description=description;
        this.tasks=tasks;
    }
    public Section(String title,String description) {
        this.title=title;
        this.description=description;
        tasks=new ArrayList<>();
    }
    public Section(int id,String title,String description,JSONArray jsonTasks) throws JSONException, ParseException {
        this.id=id;
        this.title=title;
        this.description=description;
        this.tasks=new ArrayList<>();
        for(int i=0;i<jsonTasks.length();i++) {
            JSONObject j=(JSONObject) jsonTasks.get(i);
            tasks.add(new Task(j));
        }
    }

    protected Section(Parcel in) {
        tasks = in.createTypedArrayList(Task.CREATOR);
        title = in.readString();
        description = in.readString();
    }

    public static final Creator<Section> CREATOR = new Creator<Section>() {
        @Override
        public Section createFromParcel(Parcel in) {
            return new Section(in);
        }

        @Override
        public Section[] newArray(int size) {
            return new Section[size];
        }
    };

    public void setTask(JSONArray jsonArray) throws JSONException, ParseException {
        for(int i=0;i<jsonArray.length();i++) addTask(new Task((JSONObject)jsonArray.get(i)));
    }
    public void addTask(Task t) {
        tasks.add(t);
    }
    public void removeTask(String title) {
        int i=0;
        for(Task t:tasks) {
            if(title.equals(t.title)) {
                tasks.remove(i);break;
            }
            i++;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeSerializable(tasks);
    }
    @Override
    public String toString() {
        String string="SECTION "+title+"\n";
        for(Task t:tasks) {
            string+=t;
        }
        return string;
    }
}
