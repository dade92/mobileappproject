package it.dade.mobileapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by dade on 23/12/2015.
 * This class represents an answer to a
 * question
 */
public class Answer implements Parcelable{

    public int id;
    public String body;
    public Date creationDate;
    public int score;

    public Answer(int id,String body,long timestamp,int score) {
        this.id=id;
        this.body=android.text.Html.fromHtml(body).toString();
        this.creationDate=new Date(timestamp);
        this.score=score;
    }

    protected Answer(Parcel in) {
        id = in.readInt();
        body = in.readString();
        score = in.readInt();
        creationDate=new Date(in.readLong());
    }

    public static final Creator<Answer> CREATOR = new Creator<Answer>() {
        @Override
        public Answer createFromParcel(Parcel in) {
            return new Answer(in);
        }

        @Override
        public Answer[] newArray(int size) {
            return new Answer[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(body);
        parcel.writeSerializable(creationDate);
        parcel.writeInt(score);
        parcel.writeLong(creationDate.getTime());
    }
}
