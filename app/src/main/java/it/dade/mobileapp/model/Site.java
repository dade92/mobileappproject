package it.dade.mobileapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by dade on 17/02/2016.
 * Class that represents a site of
 * the project
 */
public class Site implements Parcelable {
    public int id;
    public String siteName;

    public Site(int id,String siteName) {
        this.id=id;
        this.siteName=siteName;
    }
    public Site(JSONObject j) throws JSONException {
        this.id=j.getInt("ID");
        this.siteName=j.getString("siteName");
    }

    protected Site(Parcel in) {
        id = in.readInt();
        siteName = in.readString();
    }

    public static final Creator<Site> CREATOR = new Creator<Site>() {
        @Override
        public Site createFromParcel(Parcel in) {
            return new Site(in);
        }

        @Override
        public Site[] newArray(int size) {
            return new Site[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(siteName);
    }
    public String toString() {
        return "siteName:"+siteName+"\n";
    }
}
