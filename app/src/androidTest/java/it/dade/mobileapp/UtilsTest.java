package it.dade.mobileapp;
import org.junit.Test;

import java.util.Date;
import java.util.regex.Pattern;

import it.dade.mobileapp.utils.Utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by dade on 12/02/2016.
 */
public class UtilsTest {


    @Test
    public void parseDateTest() {
        assertEquals("11/2/2016", Utils.parseDate(new Date(1455177335)));
    }

}
