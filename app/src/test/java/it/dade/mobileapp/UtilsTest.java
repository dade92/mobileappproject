package it.dade.mobileapp;

import android.util.Log;

import org.junit.Test;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import it.dade.mobileapp.utils.Utils;

import static org.junit.Assert.*;

/**
 * Class to test Utils static methods
 */
public class UtilsTest {
    private static final String TAG=UtilsTest.class.getCanonicalName();
    @Test
    public void parseDateTest() {
        //replace with today date
        assertEquals("17/02/16", Utils.parseDate(Calendar.getInstance().getTime()));
    }
    @Test
    public void parseDateDBTest() {
        //replace with today date
        assertEquals("16-02-17", Utils.parseDateForDB(Calendar.getInstance().getTime()));
    }
    @Test
    public void validateEmailTest() {
        assertEquals(false,Utils.validateEmail("dade@.it"));
        assertEquals(false,Utils.validateEmail("@.it"));
        assertEquals(false,Utils.validateEmail("dade@.i"));
        assertEquals(false,Utils.validateEmail("dade@hotmail"));
        assertEquals(false,Utils.validateEmail("dade@hotmail."));
        assertEquals(false,Utils.validateEmail(".it"));
        assertEquals(false,Utils.validateEmail("abcd"));
        assertEquals(false,Utils.validateEmail("abcd.it"));
        assertEquals(true,Utils.validateEmail("dade@hotmail.it"));
    }
    @Test
    public void validatePasswordTest() {
        assertEquals(false,Utils.validatePassword("java"));
        assertEquals(false,Utils.validatePassword("java92"));
        assertEquals(false,Utils.validatePassword("922"));
        assertEquals(false,Utils.validatePassword("922D"));
        assertEquals(false,Utils.validatePassword("123456789aa"));
        assertEquals(false,Utils.validatePassword("12345F&"));
        assertEquals(false,Utils.validatePassword("123ABC"));
        assertEquals(false,Utils.validatePassword("123ABCD"));
        assertEquals(true,Utils.validatePassword("123456Da"));
    }
    @Test
    public void sha256EncryptionTest() {
        assertEquals("5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8",
                Utils.sha256Encryption("password"));
        assertEquals("5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5",
                Utils.sha256Encryption("12345"));
    }

}