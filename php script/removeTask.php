<?php
//TESTED
include "Database.php";
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$database=new Database();
	$database->connect();
    //get the file
 	$input = file_get_contents('php://input');
  	//get the post parameters
    if(count($_POST)!=4) echo "Bad request:strange number of parameters";
    else {
    	$username=$_POST["username"];
    	$password=$_POST["password"];
        $task_id=$_POST["task_id"];
        $project_id=$_POST["project_id"];
        if(empty($task_id) || empty($project_id)) echo "Bad request:missing parameters";
        else
    		//insert the user
    		echo $database->removeTask($username,$password,$task_id,$project_id);
    }
  }
?>