<?php
//TESTED
include "Database.php";
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$database=new Database();
	$database->connect();
    //get the file
 	$input = file_get_contents('php://input');
  	//get the post parameters
    if(count($_POST)!=5) echo "Bad request:strange number of parameters";
    else {
    	$username=$_POST["username"];
    	$password=$_POST["password"];
        $title=$_POST["title"];
        $description=$_POST["description"];
        $project_id=$_POST["project_id"];
        if(empty($title) || empty($description) || empty($project_id)) 
        	echo "Bad request:missing parameters";
        else 
    		//insert the user
    		echo $database->insertSection($username,$password,
        		$title,$description,$project_id);
    }
    }
?>