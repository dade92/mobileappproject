<?php
//TESTED
include "Database.php";
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$database=new Database();
	$database->connect();
    //get the file
 	$input = file_get_contents('php://input');
  	//get the post parameters
    if(count($_POST)!=8) echo "Bad request:strange number of parameters:".count($_POST);
    else {
    	$username=$_POST["username"];
    	$password=$_POST["password"];
        $title=$_POST["title"];
        $description=$_POST["description"];
        $priority=$_POST["priority"];
        $deadline=$_POST["deadline"];
        $section_id=$_POST["section_id"];
        $project_id=$_POST["project_id"];
        if(empty($title) || empty($section_id) || empty($project_id)
        		|| empty($description) || empty($priority)) echo "Bad request:missing parameters";
        else
    		//insert the user
    		echo $database->insertTask($username,$password,$title,$description,$priority,$deadline,$section_id,$project_id);
    }
    }
?>