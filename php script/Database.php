<?php
class Database {
 // connection parameters
 private $nomehost = "localhost";     
 private $nomeuser = "my_teamflow";          
 private $password = ""; 
 //true if the connection was established,false otherwise
 private $attiva = false;
 // connection to DB
 public function connect() {
 	if(!$this->attiva) {
   		$connection = mysql_connect($this->nomehost,$this->nomeuser,$this->password);
    	$attiva=true;
    	return true;
    } else return false;
 }    
 // close of connection
 public function disconnect() {
 	if($this->attiva) {
    	if(mysql_close()) {
        	$this->attiva = false; 
            return true; 
        } else return false; 
    }
 }
 //function used to insert a new user in the system (=registration)
 public function insertUser($username,$password,$email) {
 	if(isset($this->attiva)) {
    	$sql = "INSERT INTO `my_teamflow`.`User` (`username`,`password` ,`email`) VALUES ('$username','$password', '$email');";
        return $this->update($sql);
    }
 }
 //check if the given user exists. Returns false or the user credentials
 public function checkUserCredentials($username,$password) {
 	if(isset($this->attiva)) {
    	$sql = $sql = "SELECT * FROM `my_teamflow`.`User` WHERE `username` LIKE '$username' AND `password` LIKE '$password' ";
        return $this->singleOutputQuery($sql);
    }
 }
 //inserts a new task of the project
 public function insertTask($username,$password,$title,$description,
 											$priority,$deadline,$section_id,$project_id) {
 	 	if(isset($this->attiva)) {
        	$user=$this->checkUserCredentials($username,$password);
        	if(strcmp ($user,"false")!==0) {
            	$jsonObject=json_decode($user,true);
            	$user_id=$jsonObject['ID'];
    			if($this->isMember($project_id,$user_id)) {
    				$sql = "INSERT INTO `my_teamflow`.`Task` 
        			(`title`,`description` ,`priority`,`deadline`,`section_ID`) 
        			VALUES ('$title','$description', '$priority','$deadline','$section_id');";
        			$this->update($sql);
                    $fetchID="SELECT ID FROM `my_teamflow`.`Task` WHERE title LIKE '$title'
                    	AND section_ID LIKE '$section_id'";
                    $result=$this->singleOutputQuery($fetchID);
                    return $result;
            	} else return "Not a member";
          	} else return "Login failed";
    	}
 }
 //check if the user is the member of the project
 private function isMember($project_id,$user_id) {
 	//first check if the user is the admin
    $sql2="SELECT * FROM `my_teamflow`.`ProjectBoard` WHERE ID='$project_id'";
    $result=json_decode($this->query($sql2),true);
    if($result[0]['admin_ID']==$user_id) return true;
    //if he's not, check if the user is a member
    else {
     	$sql="SELECT * FROM `my_teamflow`.`Joined` WHERE projectBoard_ID='$project_id' AND user_ID='$user_id'";
        $result1=$this->query($sql);
        if(strcmp($result1,"null")==0)return false;
        return true;
    }
 }
 //check if the user is the admin
 private function isAdmin($project_id,$user_id) {
    $sql2="SELECT * FROM `my_teamflow`.`ProjectBoard` WHERE ID='$project_id'";
    $result=json_decode($this->query($sql2),true);
    if($result[0]['admin_ID']==$user_id) return true;
    return false;
 }
 //function used to insert a new section to the given project
 public function insertSection($username,$password,$title,$description,$project_id) {
  	 	if(isset($this->attiva)) {
        	$user=$this->checkUserCredentials($username,$password);
        	if(strcmp ($user,"false")!==0) {
            	$jsonObject=json_decode($user,true);
            	$user_id=$jsonObject['ID'];
            	if($this->isMember($project_id,$user_id)) {
    				$sql = "INSERT INTO `my_teamflow`.`Section` 
        			(`title`,`description` ,`projectBoard_ID`) 
        			VALUES ('$title','$description', '$project_id');";
        			$this->update($sql);
                    $fetchID="SELECT ID FROM `my_teamflow`.`Section` WHERE title LIKE '$title'
                    	AND projectBoard_ID LIKE '$project_id'";
                    $result=$this->singleOutputQuery($fetchID);
                    return $result;
                } else return "Not a member";
            } else return "Login failed";
    	}
 }
 //used to insert a new project in the database (to be improved!!!)
 public function insertProject($username,$password,$title,$description) {
   	 	if(isset($this->attiva)) {
        	$jsonString=$this->checkUserCredentials($username,$password);
        	if(strcmp ($jsonString,"false")!==0) {
            	$jsonObject=json_decode($jsonString,true);
            	$admin_id=$jsonObject['ID'];
    			$sql = "INSERT INTO `my_teamflow`.`ProjectBoard` 
        		(`title`,`description`,`admin_ID`) 
        		VALUES ('$title','$description','$admin_id');";
        		return $this->update($sql);
            } else return "Login failed";
    	} else return "Connection seems not working";
 }
 public function addInvitationToProject($username,$password,$project_id,$member_name) {
 	if(isset($this->attiva)) {
        $jsonString=$this->findUser($member_name);
        if(strcmp ($jsonString,"false")!==0) {
            $jsonObject=json_decode($jsonString);
            $member_id=$jsonObject->{'ID'};		//WRONG
    		$sql = "INSERT INTO `my_teamflow`.`Invited` 
        	(`projectBoard_ID`,`user_ID`) 
        	VALUES ('$project_id','$member_id');";
        	return $this->update($sql);
        } else return "User not found";
    }
 }
 //adds a specific member to the given project
 public function addMemberToProject($username,$password,$project_id,$memberName) {
  	if(isset($this->attiva)) {
       	$jsonString=$this->checkUserCredentials($username,$password);
        if(strcmp ($jsonString,"false")!==0) {
        	$jsonObject=json_decode($jsonString,true);
            $user_id=$jsonObject['ID'];
        	if($this->isAdmin($project_id,$user_id)) {
            	$sql="SELECT * FROM `my_teamflow`.`User` WHERE username='$memberName'";
                $object=$this->singleOutputQuery($sql);
                if(strcmp ($object,"false")!==0) {
                	$object=json_decode($object,true);
                	$member_id=$object['ID'];
    				$sql = "INSERT INTO `my_teamflow`.`Joined` 
        			(`projectBoard_ID`,`user_ID`) 
        			VALUES ('$project_id','$member_id');";
        			$this->removeInvitation($project_id,$member_id);
        			return $this->update($sql);
                } else return "User does not exists";
        	} else return "Not the admin";
        } else return "Login failed";
    } else return "Connection error";
 }
 //remove a member from the project
 public function removeMemberFromProject($username,$password,$project_id,$member_id) {
   	if(isset($this->attiva)) {
    	$jsonString=$this->checkUserCredentials($username,$password);
        if(strcmp ($jsonString,"false")!==0) {
        	$jsonObject=json_decode($jsonString,true);
            $user_id=$jsonObject['ID'];
        	if($this->isAdmin($project_id,$user_id)) {
    			$sql = "DELETE FROM `my_teamflow`.`Joined` WHERE
        		`projectBoard_ID`='$project_id' AND `user_ID`='$member_id';";
        		return $this->update($sql);
            } else return "Not the admin";
        } else return "Login failed";
    } else return "Connection error";
 }
 public function removeInvitation($project_id,$member_id) {
   	if(isset($this->attiva)) {
    	$sql = "DELETE FROM `my_teamflow`.`Invited` 
        	WHERE `projectBoard_ID`='$project_id' AND `user_ID`='$member_id';";
        return $this->update($sql);
    } else return "Connection error";
 }
 //function that removes a task
 public function removeTask($username,$password,$task_id,$project_id) {
 	if(isset($this->attiva)) {
    	$jsonString=$this->checkUserCredentials($username,$password);
        if(strcmp ($jsonString,"false")!==0) {
        	$jsonObject=json_decode($jsonString,true);
            $user_id=$jsonObject['ID'];
        	if($this->isMember($project_id,$user_id)) {
    			$sql = "DELETE FROM `my_teamflow`.`Task` 
        			WHERE `ID`='$task_id';";
        		return $this->update($sql);
            } else return "Not a member";
        } else return "Login failed";
    } else return "Connection error";
 }
 //removes a given section
 public function removeSection($username,$password,$section_id,$project_id) {
  	if(isset($this->attiva)) {
    	$jsonString=$this->checkUserCredentials($username,$password);
        if(strcmp ($jsonString,"false")!==0) {    	
            $jsonObject=json_decode($jsonString,true);
            $user_id=$jsonObject['ID'];
        	if($this->isMember($project_id,$user_id)) {
    			$sql = "DELETE FROM `my_teamflow`.`Section` 
        			WHERE `ID`='$section_id';";
            	$sql2="DELETE FROM `my_teamflow`.`Task` 
        			WHERE `section_ID`='$section_id';"; 
            	$this->update($sql2);
            	return $this->update($sql);
            } else return "Not a member";
        } else return "Login failed";
    } else return "Connection error";
 }
 private function fetchTasks($section_id) {
 	if(isset($this->attiva)) {
    	$sql = $sql = "SELECT * FROM `my_teamflow`.`Task` WHERE `section_ID`='$section_id'";
        return $this->query($sql);
    }
 }
 //returns all the sites
 public function allSites($username,$password) {
   	if(isset($this->attiva)) {
    	$jsonString=$this->checkUserCredentials($username,$password);
        if(strcmp ($jsonString,"false")!==0) {    	
     		$sql="SELECT * FROM `my_teamflow`.`Site`";
     		return $this->query($sql);
        } else return "Login failed";
    } else return "Connection error";
 } 
 //used to remove the user from the project
 public function leaveProject($username,$password,$project_id) {
   	if(isset($this->attiva)) {
    	$jsonString=$this->checkUserCredentials($username,$password);
        if(strcmp ($jsonString,"false")!==0) {    	
            $jsonObject=json_decode($jsonString,true);
            $user_id=$jsonObject['ID'];
        	if($this->isMember($project_id,$user_id)) {
    			$sql = "DELETE FROM `my_teamflow`.`Joined` 
        			WHERE `user_ID`='$user_id';";
            	return $this->update($sql);
            } else return "Not a member";
        } else return "Login failed";
    } else return "Connection error";
 }
 public function removeProject($username,$password,$project_id) {
 	if(isset($this->attiva)) {
    	$jsonString=$this->checkUserCredentials($username,$password);
        if(strcmp ($jsonString,"false")!==0) {    	
            $jsonObject=json_decode($jsonString,true);
            $user_id=$jsonObject['ID'];
        	if($this->isAdmin($project_id,$user_id)) {
    			$sql = "DELETE FROM `my_teamflow`.`Joined` 
        			WHERE `projectBoard_ID`='$project_id';";
            	$this->update($sql);
                $sql2="SELECT * FROM `my_teamflow`.`Section` WHERE projectBoard_id='$project_id'";
                $result=json_decode($this->query($sql2),true);
                for($i=0;$i<count($result);$i++){ 
                	$this->removeSection($username,$password,$result[$i]['ID'],$project_id);}
                $removeSql="DELETE FROM `my_teamflow`.`ProjectBoard` 
        			WHERE ID='$project_id';";
                return $this->update($removeSql);
            } else return "Not the admin";
        } else return "Login failed";
    } else return "Connection error";	
 }
 public function fetchProject($username,$password,$title) {
   	if(isset($this->attiva)) {
    	$user=$this->checkUserCredentials($username,$password);
        if(strcmp ($user,"null")!==0) {
        	    $jsonObject=json_decode($user,true);
            	$user_id=$jsonObject['ID'];
                $sql="SELECT * FROM `my_teamflow`.`ProjectBoard` WHERE title LIKE '$title'";
            	$project=$this->singleOutputQuery($sql);
                $project=json_decode($project,true);
                $project_id=$project['ID'];
            	if($this->isMember($project_id,$user_id)) 
                	return $this->fetchSectionsAndMembers($project);
                else return "Not a member";
        } else return "Login failed";
    } else return "Connection error";
 }
 private function fetchSectionsAndMembers($project) {
    $id=$project['ID'];
    $admin_id=$project['admin_ID'];
  	if(isset($this->attiva)) {
    	$sql = "SELECT * FROM `my_teamflow`.`Section` WHERE projectBoard_ID='$id'";
        $jsonObject=json_decode($this->query($sql),true);
        $projectArray=array();
        $projectArray[0]=$project;
        $taskArray=array();
        $siteArray=array();
        for($i=0; $i<count($jsonObject); $i++) {
        	$tempArray=array();
            $tempArray[0]=$jsonObject[$i];
            $tempArray[1]=json_decode($this->fetchTasks($jsonObject[$i]['ID']));
        	$taskArray[]=$tempArray;
        }
        $memberArray=json_decode($this->fetchMembers($project),true);
        $siteArray=json_decode($this->fetchSites($project),true);
        $projectArray[1]=$taskArray;
        $projectArray[2]=$memberArray;
        $projectArray[3]=$siteArray;
    	return json_encode($projectArray);
    }
 }
 private function fetchMembers($project) {
   	if(isset($this->attiva)) {
    	$project_id=$project['ID'];
        $admin_id=$project['admin_ID'];
    	$sql = "SELECT * FROM `my_teamflow`.`Joined` WHERE projectBoard_ID='$project_id'";
        $jsonObject=json_decode($this->query($sql),true);
        $userArray=array();
        $userArray[]=json_decode($this->findUserById($admin_id),true);
        for($i=1; $i<=count($jsonObject); $i++) {
        	$userArray[]=json_decode($this->findUserById($jsonObject[$i-1]['user_ID']));
        }
        return json_encode($userArray);
    }
 }
 private function fetchSites($project) {
 	if(isset($this->attiva)) {
    	$project_id=$project['ID'];
    	$sql = "SELECT * FROM `my_teamflow`.`SiteAssociation` WHERE projectBoard_ID='$project_id'";
        $jsonObject=json_decode($this->query($sql),true);
        $siteArray=array();
        for($i=0; $i<count($jsonObject); $i++) {
        	$siteArray[]=json_decode($this->findSiteById($jsonObject[$i]['site_ID']));
        }
        return json_encode($siteArray);
    }
 }
 private function findSiteById($site_id) {
 	$sql = "SELECT * FROM `my_teamflow`.`Site` WHERE `ID` LIKE '$site_id'";
 	return $this->singleOutputQuery($sql);
 }
 //returns the list of the projects in which the user is involved
 public function fetchProjectList($username,$password) {
    if(isset($this->attiva)) {
    	$jsonString=$this->checkUserCredentials($username,$password);
        if(strcmp ($jsonString,"false")!==0) {   
        	$jsonObject=json_decode($jsonString,true);
            $userId=$jsonObject['ID'];  
            //checks the projects in which he's a member
    		$sql="SELECT * FROM `my_teamflow`.`Joined` WHERE user_ID='$userId'";
            $projectBoads=$this->query($sql);
            $projects=json_decode($projectBoads,true);
            $array[]=array();
            for($i=0; $i<count($projects); $i++) {
        		$array[$i]=json_decode($this->findProjectById($projects[$i]['projectBoard_ID']));
        	}
            //now checks the projects in which he's the admin
            $sql2="SELECT * FROM `my_teamflow`.`ProjectBoard` WHERE admin_ID='$userId'";
            $projectBoads2=$this->query($sql2);		
            $array2=array();
            $array2=json_decode($projectBoads2);
            if(empty($array2)) return json_encode($array);
            $array3=array_merge($array,$array2);
        	return json_encode($array3);
        } else return "Login failed";
    } else return "Connection error";
 }
 private function findUser($username) {
    $sql = $sql = "SELECT * FROM `my_teamflow`.`User` WHERE `username` LIKE '$username'";
    return $this->query($sql);
 }
 private function findUserById($userId) {
    $sql = $sql = "SELECT * FROM `my_teamflow`.`User` WHERE `ID` LIKE '$userId'";
    return $this->singleOutputQuery($sql);
 }
 private function findProjectById($projectId) {
    $sql = "SELECT * FROM `my_teamflow`.`ProjectBoard` WHERE `ID` LIKE '$projectId'";
    return $this->singleOutputQuery($sql);
 }
  //update submission
 private function update($sql) {
  	$result = mysql_query($sql) or die (mysql_error());
  	return $result;
 }
 private function singleOutputQuery($sql) {
  	$result = mysql_query($sql) or die (mysql_error());
    return json_encode(mysql_fetch_assoc($result));
 }
 //query submission
 private function query($sql) {
  	$result = mysql_query($sql) or die (mysql_error());
  	while($row =mysql_fetch_assoc($result)) {
       	$emparray[] = $row;
    }
    return json_encode($emparray);
 }
}//end of class       
?>