<?php
include "Database.php";
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$database=new Database();
	$database->connect();
    //get the file
 	$input = file_get_contents('php://input');
  	//get the post parameters
    if(count($_POST)!=3) echo "Bad request:strange number of parameters";
    else {
    	$username=$_POST["username"];
    	$password=$_POST["password"];
        $userID=$_POST["user_id"];
        if(empty($userID)) echo "Bad request:missing parameters";
        else
    		//insert the project
    		echo $database->fetchProjectList($username,$password,$userID);
    }
    }
?>